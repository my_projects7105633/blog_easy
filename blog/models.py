from enum import unique
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse


class Post(models.Model):

    class Status(models.TextChoices):
        PUBLISHED = 'PB', 'Published'
        DRAFT = 'DF', 'Draft'

    title = models.CharField(max_length = 200,
                             unique_for_date='created')
    author = models.ForeignKey(User,
                               on_delete=models.CASCADE,
                               related_name='posts')
    body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    publish = models.DateTimeField(default=timezone.now)
    status = models.CharField(max_length=2,
                              default=Status.DRAFT,
                              choices=Status.choices)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('blog:detail',
                       args=[str(self.id)])
